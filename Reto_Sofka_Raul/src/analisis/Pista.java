package analisis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;

public class Pista {
	List<String> listaPilotos = new ArrayList<String>();
	List<String> autosDisponibles = new ArrayList<String>();
	List<String> pilotosAutos = new ArrayList<String>();
	String[] array = {"", "", "", "", "", ""};
	String[] array1 = {"", "", "", "", "", ""};
	private int matriz[][]={{1,0},{2,0},{3,0},{4,0},{5,0},{6,0}};
	public String[][] Ganadores;
	public boolean hayGanador = false;
	public int avanz;
	public int ValorDado;
	
	public static void main(String[] args) {
	}
	
	public void asignarConductor(String nombre, String conductor, int CantidadPilotos, Piloto jugador, AutosDisponible autoDisponible, String Piloto_Auto) {
	    if (CantidadPilotos < 6)
	    {
		    listaPilotos.add(nombre);
		    autosDisponibles.add(conductor);
		    pilotosAutos.add(Piloto_Auto);
		    JOptionPane.showMessageDialog(null, nombre + " se ha registrado exitosamente para la carrera, OK para continuar");
	    } else {
	        JOptionPane.showMessageDialog(null, "Se han registrado todos los pilotos para la carrera, Start para dar inicio");
	    }
	    asignarPiloto(jugador);
	    asignarAuto(autoDisponible);
	}
		
	public void asignarPiloto(Piloto piloto){
		for (int i = 0; i < listaPilotos.size(); i++) 
		{
			array[i] = listaPilotos.get(i);
		}
		piloto.setPilotos(array);
	}
	
	public void asignarAuto(AutosDisponible autoDisponible){
        for (int i = 0; i < autosDisponibles.size(); i++) {
        	array1[i] = autosDisponibles.get(i);
        }
        autoDisponible.setautosDisponibles(array1);
	}
	
    public int avanzar(int Auto, Dados dados){
        ValorDado = dados.LanzarDado(1, 6);
        avanz = Auto + ValorDado * 100;
        return avanz;
    }
    
    public void listaCompCarros(Automoviles automoviles, Dados dados){
        for (int i = 0; i < 6; i++){
			for(int j = 0; j < 2; j++){
				if(j==1){
					switch(matriz[i][0]){
						case 1 -> matriz[i][j] = automoviles.getFerrari();
						case 2 -> matriz[i][j] = automoviles.getLamborghini();
						case 3 -> matriz[i][j] = automoviles.getMclaren();
						case 4 -> matriz[i][j] = automoviles.getAudi();
						case 5 -> matriz[i][j] = automoviles.getMaserati();
						case 6 -> matriz[i][j] = automoviles.getFord();
					}
				}
			}
        }
        ordenarCarros(dados);
    }
    
    private void ordenarCarros(Dados dados){
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 2; j++){
                for(int q = 0; q < 3; q++){
                    if (matriz[q][1] < matriz[q + 1][1]){
                        int tmpc1 = matriz[q + 1][1];
                        int tmpc0 = matriz[q + 1][0];
                        matriz[q+1][1] = matriz[q][1];
                        matriz[q+1][0] = matriz[q][0];
                        matriz[q][1] = tmpc1;
                        matriz[q][0] = tmpc0;
                    }
                }
            }
        }
        asignarOrdenCarros(dados);
    }
    
    public void asignarOrdenCarros(Dados dado){
        String Matriztemp[][] = {{"",""},{"",""},{"",""},{"",""},{"",""},{"",""}};
        for (int i = 0; i < 6; i++){
        	for(int j = 0;  j < 2; j++){
        		if(j == 0){
        			switch(matriz[i][0]){
        			case 1 -> Matriztemp[i][j] = "FERRARI";
        			case 2 -> Matriztemp[i][j] = "LAMBORGHINI";
        			case 3 -> Matriztemp[i][j] = "MCLAREN";
        			case 4 -> Matriztemp[i][j] = "AUDI";
        			case 5 -> Matriztemp[i][j] = "MASERATI";
        			case 6 -> Matriztemp[i][j] = "FORD";
        		}
        	}
        	if(j == 1){
        		Matriztemp[i][j] = String.valueOf(matriz[i][j]);
        	}
   		}
   	}
        dado.setPosicionesDeCarrera(Matriztemp);
        cargarPrimerosLugares(dado);
    }
    
    public String[][] cargarPrimerosLugares(Dados dado){
        if(matriz[2][1] > 5000){
        	Ganadores = dado.getPosicionesDeCarrera();
        	hayGanador = true;
        }
        return Ganadores;
    }
}