package analisis;

public class Automoviles {


	private int Marca_Ferrari;
    private int Marca_Lamborghini;
    private int Marca_Mclaren;
    private int Marca_Audi;
    private int Marca_Maserati;
    private int Marca_Ford;
        
    public Automoviles(){
    	
    }
    
    public int getFerrari() {
    	return Marca_Ferrari;
    }

    public void setFerrari(int mFerrari) {
    	this.Marca_Ferrari = mFerrari;
    }

    public int getLamborghini() {
    	return Marca_Lamborghini;
    }

    public void setLamborghini(int mLamborghini) {
    	this.Marca_Lamborghini= mLamborghini;
    }
    
    public int getMclaren() {
    	return Marca_Mclaren;
    }

    public void setMclaren(int mMclaren) {
    	this.Marca_Mclaren= mMclaren;
    }
    public int getAudi() {
    	return Marca_Audi;
    }

    public void setAudi(int mAudi) {
    	this.Marca_Audi = mAudi;
    }
    public int getMaserati() {
    	return Marca_Maserati;
    }

    public void setMaserati(int mMaserati) {
    	this.Marca_Maserati = mMaserati;
    }
    public int getFord() {
    	return Marca_Ford;
    }

    public void setFord(int mFord) {
    	this.Marca_Ford = mFord;
    }   
    
    public void avanzacarros(Pista pista, Dados dados, int turno){
        
            switch(turno){
                case 1: 
                    setFerrari(pista.avanzar(getFerrari(), dados));
                    break;
                case 2:
                    setLamborghini(pista.avanzar(getLamborghini(), dados));
                    break;
                case 3:
                    setMclaren(pista.avanzar(getMclaren(), dados));
                    break;
                case 4:
                    setAudi(pista.avanzar(getAudi(), dados));
                    break;
                case 5:
                    setMaserati(pista.avanzar(getMaserati(), dados));
                    break;
                case 6:
                    setFord(pista.avanzar(getFord(), dados));
                    break;
            
        }
    }
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

}
