package analisis;

import java.util.Random;

public class Dados {
	private String posicionesdecarrera[][] = {{"FERRARI",""},{"LAMBORIGHINI",""},{"MCLAREN",""},{"AUDI","",},{"MASERATI",""},{"FORD",""}};
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub	
	}
	
    public int LanzarDado(){
        Random r = new Random();
        int valorDado = r.nextInt(1 + 6) + 1;
        return valorDado;
    }
    
    public int LanzarDado(int limiteInferior, int limiteSuperior) {
        return (int) (Math.random() * limiteSuperior - 1) + limiteInferior;
    }
    
    public String[][] getPosicionesDeCarrera() {
        return posicionesdecarrera;
    }

    public void setPosicionesDeCarrera(String[][] posicionesdecarrera) {
        this.posicionesdecarrera = posicionesdecarrera;
    }
}
