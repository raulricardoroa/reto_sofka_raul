package estructura;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.table.DefaultTableModel;
import analisis.Pista;
import conexion.conectionstring;
import analisis.Dados;

import analisis.Automoviles;

public class Carrera extends javax.swing.JFrame {
	
	int nTurno = 1;
	conectionstring cnn = new conectionstring();
	Automoviles automoviles = new Automoviles();
	Pista pista = new Pista();
	Dados dados = new Dados();
	String Auto_Piloto = "";
	
	javax.swing.JLabel label_Titutlo = new javax.swing.JLabel();
	javax.swing.JLabel label1 = new javax.swing.JLabel();
	javax.swing.JLabel label2 = new javax.swing.JLabel();
	javax.swing.JLabel label3 = new javax.swing.JLabel();
	javax.swing.JLabel label4 = new javax.swing.JLabel();
	javax.swing.JLabel label_MetrosRecorridos = new javax.swing.JLabel();
	javax.swing.JLabel label_PilotoTurno = new javax.swing.JLabel();
	javax.swing.JProgressBar carril_rojo = new javax.swing.JProgressBar();
	javax.swing.JLabel label_carril_rojo = new javax.swing.JLabel();
	javax.swing.JProgressBar carril_azul = new javax.swing.JProgressBar();
	javax.swing.JLabel label_carril_azul = new javax.swing.JLabel();
	javax.swing.JProgressBar carril_amarillo = new javax.swing.JProgressBar();
	javax.swing.JLabel label_carril_amarillo = new javax.swing.JLabel();
	javax.swing.JProgressBar carril_verde= new javax.swing.JProgressBar();
	javax.swing.JLabel label_carril_verde = new javax.swing.JLabel();
	javax.swing.JProgressBar carril_negro = new javax.swing.JProgressBar();
	javax.swing.JLabel label_carril_negro = new javax.swing.JLabel();
	javax.swing.JProgressBar carril_gris = new javax.swing.JProgressBar();
	javax.swing.JLabel label_carril_gris = new javax.swing.JLabel();
	javax.swing.JLabel label_Meta = new javax.swing.JLabel();
	javax.swing.JButton boton_Dados = new javax.swing.JButton();
	javax.swing.JTable Tabla_Posiciones = new javax.swing.JTable();
	javax.swing.JScrollPane Ubicacion_Tabla = new javax.swing.JScrollPane();
	
	public Carrera() {
        initComponents();
        setSize(700, 700);
    	setLocationRelativeTo(null);
    }

	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Carrera().setVisible(true);
            }
        });
	}
	
	private void initComponents() {
	    
	    JPanel panel = new JPanel();
	    label_Titutlo.setText("Grilla de partida");
	    label1.setText("");
	    label2.setText("");
	    label3.setText("");
	    label4.setText("");
	    carril_rojo.setBackground(Color.red);
	    //carril_rojo.setForeground(Color.red);
	    propiedadesBar(carril_rojo, "rojo");
	    carril_azul.setBackground(Color.blue);
	    propiedadesBar(carril_azul, "azul");
		carril_amarillo.setBackground(Color.yellow);
		propiedadesBar(carril_amarillo, "amarillo");
		carril_verde.setBackground(Color.green);
		propiedadesBar(carril_verde, "verde");
		carril_negro.setBackground(Color.black);
		propiedadesBar(carril_negro, "negro");
		carril_gris.setBackground(Color.gray);
		propiedadesBar(carril_gris, "gris");
		
	    Tabla_Posiciones.setModel(new javax.swing.table.DefaultTableModel(
	    		new Object [][] {
	    			{"1", null, null},
	    			{"2", null, null},
	    			{"3", null, null},
	    			{"4", null, null},
	    			{"5", null, null},
	    			{"6", null, null}
	    		},
	    		new String [] {
	    			"Posici�n", "Automovil", "Metros desplazados"
	    		}
	    ){
	    	boolean[] canEdit = new boolean [] {
	    		false, true, true
	    	};
	    	public boolean isCellEditable(int rowIndex, int columnIndex) {
	    		return canEdit [columnIndex];
	    	}
	    });
	    Ubicacion_Tabla.setViewportView(Tabla_Posiciones);
	    Ubicacion_Tabla.setPreferredSize(new Dimension(400, 130));
		boton_Dados.setText("Lanzador");
		boton_Dados.addActionListener(new java.awt.event.ActionListener() {
	        public void actionPerformed(java.awt.event.ActionEvent evt) {
	            eventoClickLanzarDados(evt);
	        }
	    });	
		
		JPanel panelGrillaTitulo = new JPanel();
    	//panel.setLayout(new GridLayout(Filas, Columnas));
		panelGrillaTitulo.setLayout(new GridLayout(1, 3));
		panelGrillaTitulo.add(label1);
		panelGrillaTitulo.add(label_Titutlo);
		panelGrillaTitulo.add(label2);
		getContentPane().add(panelGrillaTitulo);
				
		JPanel panelGrillaTabla = new JPanel();
    	//panel.setLayout(new GridLayout(Filas, Columnas));
		panelGrillaTabla.setLayout(new GridLayout(1, 3));
		panelGrillaTabla.add(label3);
		panelGrillaTabla.add(Ubicacion_Tabla);
		panelGrillaTabla.add(label4);
		getContentPane().add(panelGrillaTabla);	
			
	    getContentPane().setLayout(new FlowLayout());
		getContentPane().add(carril_rojo);
		getContentPane().add(label_carril_rojo);
		getContentPane().add(carril_azul);
		getContentPane().add(label_carril_azul);
		getContentPane().add(carril_amarillo);
		getContentPane().add(label_carril_amarillo);
		getContentPane().add(carril_verde);
		getContentPane().add(label_carril_verde);
		getContentPane().add(carril_negro);
		getContentPane().add(label_carril_negro);
		getContentPane().add(carril_gris);
		getContentPane().add(label_carril_gris);
		getContentPane().add(label_MetrosRecorridos);
		getContentPane().add(label_PilotoTurno);	
		getContentPane().add(boton_Dados);		
	}
	
	private void propiedadesBar(JProgressBar carril, String textoCarril) {
		carril.setMaximum(5000);
		carril.setOrientation(0);
		carril.setString("Carril " + textoCarril);
		carril.setStringPainted(true);
	}
	
	private void eventoClickLanzarDados(java.awt.event.ActionEvent evt) {
		switch(nTurno) {
			case 1 -> Auto_Piloto = "FERRARI";
			case 2 -> Auto_Piloto = "LAMBORGHINI";
			case 3 -> Auto_Piloto = "MCLAREN";
			case 4 -> Auto_Piloto = "AUDI";
			case 5 -> Auto_Piloto = "MASERATI";
			case 6 -> Auto_Piloto = "FORD";
		}
		
		label_PilotoTurno.setText(Auto_Piloto);
		automoviles.avanzacarros(pista, dados, nTurno);
		
		carril_rojo.setValue(automoviles.getFerrari());
		label_carril_rojo.setText(String.valueOf(automoviles.getFerrari()) + " metros");
		carril_azul.setValue(automoviles.getLamborghini());
		label_carril_azul.setText(String.valueOf(automoviles.getLamborghini()) + " metros");
		carril_amarillo.setValue(automoviles.getMclaren());
		label_carril_amarillo.setText(String.valueOf(automoviles.getMclaren()) + " metros");
		carril_verde.setValue(automoviles.getAudi());
		label_carril_verde.setText(String.valueOf(automoviles.getAudi()) + " metros");
		carril_negro.setValue(automoviles.getMaserati());
		label_carril_negro.setText(String.valueOf(automoviles.getMaserati()) + " metros");
		carril_gris.setValue(automoviles.getFord());
		label_carril_gris.setText(String.valueOf(automoviles.getFord()) + " metros");
		
		label_MetrosRecorridos.setText(String.valueOf(pista.ValorDado * 100) + " Metros");
		
		pista.listaCompCarros(automoviles, dados);
		
		if(nTurno < 6) {
			nTurno = nTurno + 1;
		}
		else {
			nTurno = 1;
		}
		
		for(int i = 0; i < 6; i++){
			for (int j = 0; j < 2; j++){
				DefaultTableModel modelo =(DefaultTableModel)Tabla_Posiciones.getModel();
				modelo.setValueAt(dados.getPosicionesDeCarrera()[i][j], i, j + 1);
			}
		}
		
		if(pista.hayGanador == true){
			Premiacion abrirPremiacion = new Premiacion(pista);
			abrirPremiacion.setVisible(true);
			cnn.insert(dados, pista);
			this.setVisible(false);
		}
	}
}
