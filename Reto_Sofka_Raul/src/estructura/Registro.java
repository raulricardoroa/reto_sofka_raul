package estructura;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import analisis.AutosDisponible;
import analisis.Pista;
import analisis.Piloto;

public class Registro extends javax.swing.JFrame {
	
	Pista pista = new Pista();
	AutosDisponible autoDisponible = new AutosDisponible();
	Piloto piloto = new Piloto();
	String Ferrari = "", Lamborghini = "", McLaren = "", Audi = "", Maserati = "", Ford = "", Piloto_Auto;
	
	int CantidadPilotos = 0;
	
	private javax.swing.JLabel label_Espacio1 = new javax.swing.JLabel();
	private javax.swing.JLabel label_Espacio2 = new javax.swing.JLabel();
	private javax.swing.JLabel label_Espacio3 = new javax.swing.JLabel();
	private javax.swing.JLabel label_Espacio4 = new javax.swing.JLabel();
	private javax.swing.JLabel label_TituloFormulario = new javax.swing.JLabel();
	private javax.swing.JLabel label_IngresarNombre = new javax.swing.JLabel();
	private javax.swing.JTextArea text_IngresarNombre = new javax.swing.JTextArea();
	private javax.swing.JButton boton_AgregarPiloto = new javax.swing.JButton();
	private javax.swing.JLabel label_SeleccionarAuto = new javax.swing.JLabel();
	private javax.swing.JComboBox<String> lista_AutosDisponibles = new javax.swing.JComboBox<>();
	private javax.swing.JButton boton_Start = new javax.swing.JButton();
	
	public Registro () {
    	initComponents();
    	setSize(700, 135);
    	setLocationRelativeTo(null);
    }
	
    private void initComponents() {
    	    	
    	label_Espacio1.setText("");
    	label_Espacio2.setText("");
    	label_Espacio3.setText("");
    	label_Espacio4.setText("");
    	label_TituloFormulario.setFont(new java.awt.Font("Arial", 0, 18));
    	label_TituloFormulario.getSize();
    	label_TituloFormulario.setText("Registro        de        Pilotos");
    	label_IngresarNombre.setText("Ingresar Nombre Piloto:");
    	text_IngresarNombre.setText("");
    	label_SeleccionarAuto.setText("Seleccione un Auto:");
    	label_SeleccionarAuto.setFont(new java.awt.Font("Arial", 0, 12));
    	boton_AgregarPiloto.setText("Agregar el piloto a la carrera");
    	boton_AgregarPiloto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	ListarTodos(evt);
            }
        });
    	lista_AutosDisponibles.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"FERRARI", "LAMBORGHINI", "MCLAREN", "AUDI", "MASERATI", "FORD" }));
    	boton_Start.setText("Start");
    	boton_Start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eventoClickStart(evt);
            }
        });
    	//label_TituloFormulario.setVerticalAlignment(SwingConstants.BOTTOM);*/
    	JPanel panel = new JPanel();
    	//panel.setLayout(new GridLayout(Filas, Columnas));
    	panel.setLayout(new GridLayout(4, 3));
    	panel.add(label_Espacio1);
    	panel.add(label_TituloFormulario);
    	panel.add(label_Espacio2);
    	panel.add(label_IngresarNombre);
    	panel.add(text_IngresarNombre);
    	panel.add(boton_AgregarPiloto);
    	panel.add(label_SeleccionarAuto);
    	panel.add(lista_AutosDisponibles);
    	panel.add(label_Espacio3);
    	panel.add(label_Espacio4);
    	panel.add(boton_Start);
    	getContentPane().add(panel);
    }
        
    public static void main(String args[]) {
    	try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    	java.awt.EventQueue.invokeLater(new Runnable() {
    		public void run() {
    			new Registro().setVisible(true);
    			}
    		});
    	}
    
    private void ListarTodos(java.awt.event.ActionEvent evt) {
        CantidadPilotos = CantidadPilotos + 1;
        Piloto_Auto = text_IngresarNombre.getText() + " en " + lista_AutosDisponibles.getSelectedItem().toString();
        //pista.asignarConductor(text_IngresarNombre.getText(), lista_AutosDisponibles.getSelectedItem().toString(), CantidadPilotos, piloto, autoDisponible);
        pista.asignarConductor(text_IngresarNombre.getText(), lista_AutosDisponibles.getSelectedItem().toString(), CantidadPilotos, piloto, autoDisponible, Piloto_Auto);
    }
    
    private void eventoClickStart(java.awt.event.ActionEvent evt) {    
    	Carrera abrir = new Carrera();
    	abrir.setVisible(true);
    	this.setVisible(false);
    }
}
