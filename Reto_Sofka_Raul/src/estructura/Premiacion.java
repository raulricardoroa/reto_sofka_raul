package estructura;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.text.JTextComponent;
import analisis.Pista;

public class Premiacion extends javax.swing.JFrame {
	
	javax.swing.JLabel text_Campeon = new javax.swing.JLabel();
	javax.swing.JLabel text_SubCampeon= new javax.swing.JLabel();
	javax.swing.JLabel text_Tercero = new javax.swing.JLabel();
	javax.swing.JLabel text_Campeon1 = new javax.swing.JLabel();
	javax.swing.JLabel text_SubCampeon1= new javax.swing.JLabel();
	javax.swing.JLabel text_Tercero1 = new javax.swing.JLabel();
	javax.swing.JPanel panel_Pedestal = new javax.swing.JPanel();
	

    private Premiacion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
	
    public Premiacion(Pista pista) {
        initComponents();
		text_Campeon.setText(pista.Ganadores[0][0]);
    	text_SubCampeon.setText(pista.Ganadores[1][0]);
    	text_Tercero.setText(pista.Ganadores[2][0]);
        setSize(250, 250);
    	setLocationRelativeTo(null);
    	text_Campeon1.setText("Primer puesto: "); 
    	text_SubCampeon1.setText("Segundo puesto: ");
    	text_Tercero1.setText("Tercer puesto: ");
    	
    }
    
    public void initComponents() {
    	text_Campeon.setFont(new java.awt.Font("Calibri", 1, 18));
    	text_SubCampeon.setFont(new java.awt.Font("Calibri", 1, 18));
    	text_Tercero.setFont(new java.awt.Font("Calibri", 1, 18));
    	JPanel panel = new JPanel(); 
    	panel.setLayout(new GridLayout(3, 2));
    	//panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    	panel.add(text_Campeon1);
    	panel.add(text_Campeon);
    	panel.add(text_SubCampeon1);
    	panel.add(text_SubCampeon);
    	panel.add(text_Tercero1);
    	panel.add(text_Tercero);
    	getContentPane().add(panel);
    }

	public static void main(String[] args) {
	      try {
	            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
	                if ("Nimbus".equals(info.getName())) {
	                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
	                    break;
	                }
	            }
	        } catch (ClassNotFoundException ex) {
	            java.util.logging.Logger.getLogger(Premiacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (InstantiationException ex) {
	            java.util.logging.Logger.getLogger(Premiacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (IllegalAccessException ex) {
	            java.util.logging.Logger.getLogger(Premiacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
	            java.util.logging.Logger.getLogger(Premiacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        }
	        //</editor-fold>

	        /* Create and display the form */
	        java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run() {
	                new Premiacion().setVisible(true);
	            }
	        });

	}
}
